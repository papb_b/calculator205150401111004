package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


    Button button0, button1, button2, button3, button4, button5, button6,
            button7, button8, button9, buttonPlus, buttonMin, buttonDivide,
            buttonTimes, buttonClear, buttonEqual;
    EditText operationEditText;

    int number1, number2;
    String operationInput = "";
    boolean operationAddition, operationSubbtraction, operationMultiplication, operationDivision;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        button0 = (Button) findViewById(R.id.button0);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        button5 = (Button) findViewById(R.id.button5);
        button6 = (Button) findViewById(R.id.button6);
        button7 = (Button) findViewById(R.id.button7);
        button8 = (Button) findViewById(R.id.button8);
        button9 = (Button) findViewById(R.id.button9);
        buttonPlus = (Button) findViewById(R.id.buttonPlus);
        buttonMin = (Button) findViewById(R.id.buttonMin);
        buttonTimes = (Button) findViewById(R.id.buttonTimes);
        buttonDivide = (Button) findViewById(R.id.buttonDivide);
        buttonClear = (Button) findViewById(R.id.buttonClear);
        buttonEqual = (Button) findViewById(R.id.buttonEqual);
        operationEditText = (EditText) findViewById(R.id.edt1);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationEditText.setText(operationEditText.getText() + "1");
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationEditText.setText(operationEditText.getText() + "2");
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationEditText.setText(operationEditText.getText() + "3");
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationEditText.setText(operationEditText.getText() + "4");
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationEditText.setText(operationEditText.getText() + "5");
            }
        });

        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationEditText.setText(operationEditText.getText() + "6");
            }
        });

        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationEditText.setText(operationEditText.getText() + "7");
            }
        });

        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationEditText.setText(operationEditText.getText() + "8");
            }
        });

        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationEditText.setText(operationEditText.getText() + "9");
            }
        });

        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationEditText.setText(operationEditText.getText() + "0");
            }
        });

        buttonPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationInput += operationEditText.getText()+"+";
                if (operationEditText == null) {
                    operationEditText.setText("");
                } else {
                    number1 = Integer.parseInt(operationEditText.getText() + "");
                    operationAddition = true;
                    operationEditText.setText(null);
                }
            }
        });

        buttonMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationInput += operationEditText.getText()+"-";
                if (operationEditText == null) {
                    operationEditText.setText("");
                } else {
                    number1 = Integer.parseInt(operationEditText.getText() + "");
                    operationSubbtraction = true;
                    operationEditText.setText(null);
                }
            }
        });

        buttonDivide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationInput += operationEditText.getText()+"/";
                if (operationEditText == null) {
                    operationEditText.setText("");
                } else {
                    number1 = Integer.parseInt(operationEditText.getText() + "");
                    operationDivision = true;
                    operationEditText.setText(null);
                }
            }
        });

        buttonTimes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationInput += operationEditText.getText()+"*";
                if (operationEditText == null) {
                    operationEditText.setText("");
                } else {
                    number1 = Integer.parseInt(operationEditText.getText() + "");
                    operationMultiplication = true;
                    operationEditText.setText(null);
                }
            }
        });

        buttonEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationInput+=operationEditText.getText();
                number2 = Integer.parseInt(operationEditText.getText() + "");
                if (operationAddition == true) {
                    operationEditText.setText(number1 + number2 + "");
                    operationAddition = false;
                }
                if (operationSubbtraction == true) {
                    operationEditText.setText(number1 - number2 + "");
                    operationSubbtraction = false;
                }
                if (operationMultiplication == true) {
                    operationEditText.setText(number1 * number2 + "");
                    operationMultiplication = false;
                }
                if (operationDivision == true) {
                    operationEditText.setText(number1 / number2 + "");
                    operationDivision = false;
                }

                openActivity2();

                operationInput = "";
            }
        });

        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operationEditText.setText("");
            }
        });
    }


    public void openActivity2() {
        Intent intent = new Intent(this, Activity2.class);
        intent.putExtra("number",operationEditText.getText().toString());
        intent.putExtra("operationInput",operationInput);
        startActivity(intent);


    }





}
