package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    private TextView hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        String number = getIntent().getExtras().getString("number");
        String operation = getIntent().getExtras().getString("operationInput");
        hasil= findViewById(R.id.txt_hasil);
        hasil.setText("Hasil operasi : "+operation+" = "+number);
    }
}